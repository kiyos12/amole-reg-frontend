import React, {Component} from 'react';
import './App.css';
import firebase from 'firebase';
import Home from "./screens/Home/Home";
import Register from "./screens/Register/Register";
import Col from 'antd/lib/col';
import Row from 'antd/lib/row';
import {Redirect, Route, Switch} from "react-router-dom";

class App extends Component {

    constructor(props) {
        super(props);
        const config = {
            apiKey: "AIzaSyDNVhz4cX5F9Gd5mCW0X1HjNBAbCjxtTjg",
            authDomain: "amole-registration.firebaseapp.com",
            databaseURL: "https://amole-registration.firebaseio.com",
            projectId: "amole-registration",
            storageBucket: "amole-registration.appspot.com",
            messagingSenderId: "1024222696172"
        };

        firebase.initializeApp(config);
    }

    render() {
        const mainLayout = {
            wrapperCol: {xs: 8, sm: 16, md: 24, lg: 32},
        };

        return (

            <Row type="flex">
                <Col  xs={{ span: 20, offset: 2 }} lg={{ span: 6, offset: 9 }} flex={1}>
                    <div className="App" {...mainLayout}>
                        <header className="App-header">
                            <img src={require('./assets/amole_logo.png')}
                                 style={{width: 200, height: 75, alignSelf: 'center'}}
                                 alt={"Amole logo"}/>
                        </header>
                        <div className="App-intro">
                            <Switch>
                                <Route exact path="/" component={Home}/>
                                <Route path="/register" component={Register}/>
                                <Redirect to="/"/>
                            </Switch>
                        </div>

                    </div>
                </Col>
            </Row>
        );
    }
}

export default App;
