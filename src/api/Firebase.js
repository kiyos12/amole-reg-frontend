import firebase from "firebase";

export default {
    requestVerificationCode: (phoneNumber) => {
        return new Promise((resolve, reject) => {
            if (phoneNumber < 10) {
                reject("Phone number incorrect")
            } else {
                firebase
                    .auth()
                    .signInWithPhoneNumber(phoneNumber, window.recaptchaVerifier)
                    .then(confirmationResult => {
                            console.log(confirmationResult);
                            resolve(confirmationResult)
                        }
                    )
                    .catch(error => {
                            console.log(error);
                            reject(error)
                        }
                    );
            }
        });
    }
}