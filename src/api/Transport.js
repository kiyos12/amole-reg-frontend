import axios from 'axios';

export default {
    regCustomer: (data) => axios({
        'url': `/amolereg`,
        'data': data,
        'method': "POST",
        'withCredentials': true
    }),
    ping: () => axios({
        'url': `http://localhost:8080/ping`,
        'method': "GET",
        'withCredentials': true
    }),
    sendSMS: (data) => axios({
        'url': `/phoneout`,
        'data': data,
        'method': "POST",
        'withCredentials': true
    }),
    verifyCode: (data) => axios({
        'url': `/verify`,
        'data': data,
        'method': "POST",
        'withCredentials': true
    }),

}