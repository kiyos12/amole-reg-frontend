import React, {Component} from 'react';
import Button from 'antd/lib/button';
import DatePicker from 'antd/lib/date-picker';
import Form from 'antd/lib/form';
import Input from 'antd/lib/input';
import Modal from 'antd/lib/modal';
import notification from 'antd/lib/notification';
import Select from 'antd/lib/select';
import Typography from 'antd/lib/typography';

import Lottie from 'react-lottie';

import Cities from '../../assets/cities';
import firebase from "firebase";
import Transport from "../../api/Transport";
import successAnimation from '../../assets/successAnimation';
import {withRouter} from "react-router-dom";
import Icon from "antd/lib/icon";

const {Option} = Select;


function hasErrors(fieldsError) {
    return Object.keys(fieldsError).some(field => fieldsError[field]);
}

class Register extends Component {

    state = {
        confirmDirty: false,
        firstName: "",
        lastName: "",
        birthDate: "",
        city: "",
        phoneNumber: "",
        confirmationResult: "",
        verificationCode: "",
        visibleModal: false,
        confirmationLoading: false,
        registerLoading: false
    };

    sendSMS = (data) => {
        Transport.sendSMS(data).then(response => {
            this.setState({
                registerLoading: false
            });
            if(response.data.message === "SMS sent") {
                Register.notification("success", "Success", "SMS is sent to your number, please insert the verification code");
                this.setState({confirmationResult: response.data.message})
            }
            else{
                Register.notification("error", "Error", response.data.message)
            }
        })
        .catch(err => {
            Register.notification("error", "Error", err.message);
            this.setState({
                registerLoading: false
            });
        });
    };

    handleSubmit = (e) => {
        e.preventDefault();
        const {phoneNumber} = this.state;
        this.setState({
            registerLoading: true
        });

        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                const data = {
                    "MobileTel": `251${phoneNumber}`
                };
                this.sendSMS(data);
            } else {
                this.setState({
                    registerLoading: false
                });
            }
        });
    };
    handleConfirmationAndRegister = () => {
        const {firstName, lastName, birthDate, city, phoneNumber} = this.state;

        this.setState({
            confirmationLoading: true
        });

        const data = {
            "FirstName": firstName,
            "SecondName": lastName,
            "MobileTel": "+251" + phoneNumber,
            "City": city,
            "Country": "Ethiopia",
            "Nationality": "Ethiopian",
            "Residency": "Ethiopia",
            "BirthDate": birthDate,
            "Gender": "M"
        };

        const phoneNumberWithCode = {
            "mobileTel": `251${this.state.phoneNumber}`,
            "verificationCode": this.state.verificationCode
        };
        Transport.verifyCode(phoneNumberWithCode).then(response => {
            this.setState({
                registerLoading: false
            });
            if(response.data.message === "This validation code is correct") {
                Transport.regCustomer(data).then(response => {
                    this.setState({
                        confirmationLoading: false
                    });
                    if (response.data.ErrorCode === "10311") {
                        Register.notification("info", "Thank you", "You already have amole account")
                    } else if (response.data.ErrorCode === "00001") {
                        this.setState({
                            visibleModal: true
                        });
                        this.resetState();
                        Register.notification("success", "Success", "You are registered to Amole, we will send you 5 birr airtime soon ")
                    }
                }).catch(err => {
                    this.setState({
                        confirmationLoading: false,
                    });
                    this.resetState();
                    Register.notification("error", "Error", "Error in registering")
                });
            }
            else{
                Register.notification("error", "Error", response.data.message)
            }
        })
        .catch(err => Register.notification("error", "Error", err.message));
    };
    handleCancel = () => {
        this.setState({
            visibleModal: false,
        });
    };

    constructor(props) {
        super(props);
        this.handleFirstNameChange = this.handleFirstNameChange.bind(this);
        this.handleLastNameChange = this.handleLastNameChange.bind(this);
        this.handleBirthDateChange = this.handleBirthDateChange.bind(this);
        this.handlePhoneNumberChange = this.handlePhoneNumberChange.bind(this);
        this.handleCitySelectionChange = this.handleCitySelectionChange.bind(this);
        this.handleVerificationCodeChange = this.handleVerificationCodeChange.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
        this.handleCodeResend = this.handleCodeResend.bind(this);
    }

    static notification(type, message, description) {
        notification[type]({
            message: message,
            description: description
        })
    }

    resetState() {
        this.setState({
            confirmDirty: false,
            firstName: "",
            lastName: "",
            birthDate: "",
            city: "",
            phoneNumber: "",
            confirmationResult: "",
            verificationCode: "",
            visibleModal: false,
            confirmationLoading: false,
            registerLoading: false
        })
    }

    componentDidMount() {
        window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier(this.recaptcha, {
            'size': 'normal',
        });
        window.recaptchaVerifier.render().then(function (widgetId) {
            window.recaptchaWidgetId = widgetId;
        });
    }

    handleCityChange(value) {
        let autoCompleteResult;
        if (!value) {
            autoCompleteResult = [];
        } else {
            autoCompleteResult = Cities.cities.item.filter(city => city.toLowerCase().includes(value.toLowerCase()));
        }
        this.setState({autoCompleteResult});
    };

    handleFirstNameChange(value) {
        this.setState({firstName: value})
    };

    handleLastNameChange(value) {
        this.setState({lastName: value})
    };

    handleBirthDateChange(date, dateString) {
        this.setState({birthDate: dateString})
    };

    handlePhoneNumberChange(value) {
        this.setState({phoneNumber: value})
    };

    handleCitySelectionChange(value) {
        this.setState({city: value})
    };

    handleVerificationCodeChange(value) {
        this.setState({verificationCode: value})
    };

    handleCodeResend() {
        const data = {
            "MobileTel": `251${this.state.phoneNumber}`
        };
        this.sendSMS(data);
    }

    render() {

        const {
            getFieldDecorator, getFieldsError, getFieldError, isFieldTouched,
        } = this.props.form;

        const formItemLayout = {
            labelCol: {xs: 8, sm: 16, md: 24, lg: 32},
            wrapperCol: {xs: 8, sm: 16, md: 24, lg: 32}
        };

        const tailFormItemLayout = {
            wrapperCol: {xs: 8, sm: 16, md: 24, lg: 32},
        };

        const defaultOptions = {
            loop: false,
            autoplay: true,
            animationData: successAnimation,
            rendererSettings: {
                preserveAspectRatio: 'xMidYMid slice'
            }
        };

        const prefixSelector = getFieldDecorator('prefix', {
            initialValue: '251',
        })(
            <Select style={{width: 100}}>
                <Option value="251">+251</Option>
            </Select>
        );

        return (
            <div>
                <p style={{marginTop: '20px', fontSize: '18px'}}>
                    You just need <span className="time">30</span> sec to enjoy your mobile wallet.
                </p>

                <Modal
                    title="Successfully registered"
                    visible={this.state.visibleModal}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}>

                    <div style={{
                        display: 'flex',
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>
                        <Lottie options={defaultOptions}
                                height={200}
                                width={200}
                                isStopped={false}/>

                        <Typography.Title level={3} style={{textAlign: 'center', marginTop: 20}} type="success">You have
                            successfully Registered to Amole, We will send you your 5br mobile card</Typography.Title>
                    </div>
                </Modal>
                {
                    this.state.confirmationResult === "" ?
                        <Form {...formItemLayout} onSubmit={this.handleSubmit}>

                            <Form.Item
                                required={true}
                                label="">

                                {getFieldDecorator('firstName', {
                                    rules: [{
                                        required: true,
                                        pattern: /^[a-zA-Z]+$/,
                                        message: 'Please input valid first name!'
                                    }],
                                })(
                                    <Input size="large"
                                           prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}}/>}
                                           placeholder={"First Name"}
                                           value={this.state.firstName}
                                           onChange={e => this.handleFirstNameChange(e.target.value)}/>
                                )}
                            </Form.Item>
                            <Form.Item
                                label="">
                                {getFieldDecorator('lastName', {
                                    rules: [{
                                        required: true,
                                        pattern: /^[a-zA-Z]+$/,
                                        message: 'Please input valid last name!'
                                    }],
                                })(
                                    <Input
                                        size="large" placeholder={"Last Name"}
                                        prefix={<Icon type="key" style={{color: 'rgba(0,0,0,.25)'}}/>}
                                        onChange={e => this.handleLastNameChange(e.target.value)}/>
                                )}
                            </Form.Item>
                            <Form.Item
                                label="">
                                {getFieldDecorator('birthDate', {
                                    rules: [{required: true, message: 'Please select your birthdate!'}],
                                })(
                                    <DatePicker onChange={this.handleBirthDateChange}
                                                style={{width: '100%'}}
                                                size="large"/>
                                )}
                            </Form.Item>

                            <Form.Item
                                label="">
                                {getFieldDecorator('city', {
                                    rules: [{required: true, message: 'Please select your city!'}],
                                })(
                                    <Select
                                        size="large"
                                        showSearch
                                        placeholder="City"
                                        optionFilterProp="children"
                                        onChange={this.handleCitySelectionChange}
                                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                                    >
                                        {
                                            Cities.cities.item.map(city => {
                                                return (
                                                    <Option key={city} value={city}>{city}</Option>
                                                )
                                            })
                                        }
                                    </Select>
                                )}
                            </Form.Item>

                            <Form.Item
                                label="">
                                {getFieldDecorator('phoneNumber', {
                                    rules: [{
                                        required: true,
                                        pattern: /^9\d{8}$/,
                                        message: 'Please input valid phone number (make sure you start with 9)!'
                                    }],
                                })(
                                    <Input addonBefore={prefixSelector}
                                           size="large" style={{width: '100%'}}
                                           placeholder={"911234567"}
                                           value={this.state.phoneNumber}
                                           onChange={e => this.handlePhoneNumberChange(e.target.value)}/>
                                )}
                            </Form.Item>

                            <Form.Item {...tailFormItemLayout}>
                                <div ref={(ref) => this.recaptcha = ref}/>
                            </Form.Item>

                            <Form.Item {...tailFormItemLayout}>
                                <Button
                                    type="primary"
                                    loading={this.state.registerLoading}
                                    onClick={(e) => this.handleSubmit(e)}
                                >Register for Amole wallet</Button>
                            </Form.Item>

                        </Form> :
                        <Form {...formItemLayout}>
                            <Form.Item
                                label="">
                                <Input size="large" placeholder={"Verification Code"}
                                       value={this.state.verificationCode}
                                       onChange={e => this.handleVerificationCodeChange(e.target.value)}/>
                            </Form.Item>

                            <Form.Item {...tailFormItemLayout}>
                                <Button type="primary"
                                        htmlType="submit"
                                        disabled={hasErrors(getFieldsError())}
                                        loading={this.state.confirmationLoading}
                                        onClick={() => this.handleConfirmationAndRegister()}>Confirm</Button>
                            </Form.Item>
                        </Form>
                }
            </div>
        );
    }
}

export default withRouter(Form.create({name: 'register'})(Register));