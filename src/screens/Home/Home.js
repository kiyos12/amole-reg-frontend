import React, {Component} from 'react';
//import {Button, Carousel, Icon} from 'antd';
import Button from 'antd/lib/button';
import Carousel from 'antd/lib/carousel';
import Icon from 'antd/lib/icon';
import {withRouter} from 'react-router-dom';
import './Home.css';

class Home extends Component {
    static defaultProps = {};

    static propTypes = {};

    state = {};

    render() {
        const formItemLayout = {xs: 8, sm: 16, md: 24, lg: 32};

        return (
            <div>
                <Carousel autoplay>
                    <div>

                        <p className="carousel-text">The fun and easy way to send, pay, and receive money</p>

                        <img src={require("../../assets/onboarding_bg.jpg")} alt="" style={{width: '100%'}}/>
                    </div>
                    <div>
                        <p className="carousel-text">Buy and pay for airtime, DSTv, airlines tickets and more</p>

                        <img src={require("../../assets/onboarding_bg.jpg")} alt="" style={{width: '100%'}}/>
                    </div>
                </Carousel>

                <Button type="primary" className="continue-button-style"
                        onClick={() => this.props.history.push("/register")}>
                    I understand the benefits continue<Icon type="right"/>
                </Button>

            </div>
        );
    }
}

export default withRouter(Home);
